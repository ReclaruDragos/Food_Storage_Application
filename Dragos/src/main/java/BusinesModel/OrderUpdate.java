package BusinesModel;

import Dao.DB_Product;
import Dao.OrderDao;
import Model.Order;
import Model.Product;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class OrderUpdate  {
    private List<Order> order;
    private JdbcTemplate jdbb;
    public OrderUpdate(JdbcTemplate jdb, List<Order> o){
  jdbb=jdb;
        order=o;
    }
    //punem in baza de date doar orderurile care s au putut efectua
    public void updateOrder(){
        OrderDao da=new OrderDao();
        da.setJdbcTemplate(jdbb);
        da.deleteAll();
       for(Order i:order)
           da.saveOrder(i);

    }
}
