package BusinesModel;

import Dao.DB_Product;
import Dao.DB_user;
import Dao.OrderDao;
import Model.Order;
import Model.Product;
import Model.User;
import PresentationLawyer.OutputUser;
import org.springframework.jdbc.core.JdbcTemplate;

public class Parser { //proceseaza comenzile
    String []p;
    JdbcTemplate jdb;
    public Parser(String s,JdbcTemplate templ){
        jdb=templ;
        p=s.split("\n");
        proceseazaComenzi();

    }

    public void proceseazaComenzi(){
        String []parseComanda;
        String []usersp;
        for(String i:p){
            System.out.println(i);
            parseComanda=i.split(": ");

            if(parseComanda[0].equals("Insert client"))
            {
                usersp=parseComanda[1].split(", ");
                insertUser(new User(usersp[0],usersp[1]));


            }

            if(parseComanda[0].equals("Delete client"))
            {
                usersp=parseComanda[1].split(", ");
                deleteUser(new User(usersp[0],usersp[1]));


            }

            if(parseComanda[0].equals("Insert product"))
            {
                usersp=parseComanda[1].split(", ");
                insertProduct(new Product(usersp[0],usersp[1],usersp[2]));

            }

            if(parseComanda[0].equals("Delete Product"))
            {
                usersp=parseComanda[1].split(" ");
                deleteProduct(new Product(usersp[0],"",""));

            }

            if(parseComanda[0].equals("Order"))
            {
                usersp=parseComanda[1].split(", ");

                insertOrder(new Order(usersp[0],usersp[1],usersp[2]));

            }


            if(parseComanda[0].equals("Report client")){
                OutputUser outtable=new OutputUser(jdb);
                outtable.reportClient();


            }
            if(parseComanda[0].equals("Report order")){
                OutputUser outtable=new OutputUser(jdb);
                outtable.reportOrder();

            }
            if(parseComanda[0].equals("Report product")){
                OutputUser outtable=new OutputUser(jdb);
                outtable.reportProduct();

            }




        }
    }

    public void insertUser(User e){

        DB_user daouser=new DB_user();
        daouser.setJdbcTemplate(jdb);
        daouser.saveuser(e);

    }

    public void deleteUser(User e){
        DB_user daouser=new DB_user();
        daouser.setJdbcTemplate(jdb);
        daouser.deleteUser(e);

    }

    public void insertProduct(Product product){
        DB_Product daoprod=new DB_Product();
        daoprod.setJdbcTemplate(jdb);
        boolean op=false;
        for(Product i:daoprod.allProducts()){
            if(i.getNume().equals(product.getNume())){
                int c=Integer.valueOf(i.getCantitate())+Integer.valueOf(product.getCantitate());
                product.setCantitate(""+c);
                daoprod.updateprod(product);
                op=true;
                break;
            }
        }
        if(op==false)
        daoprod.saveProduct(product);
    }

    public void deleteProduct(Product product){
        DB_Product daoprod=new DB_Product();
        daoprod.setJdbcTemplate(jdb);
        daoprod.deleteProduct(product);

    }

    public void insertOrder(Order o){
        OrderDao ord=new OrderDao();
        ord.setJdbcTemplate(jdb);
        ord.saveOrder(o);

    }
}
