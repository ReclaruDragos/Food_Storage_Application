package BusinesModel;

import Dao.DB_Product;
import Dao.OrderDao;
import Model.Order;
import Model.Product;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

public class OrderProcessing {

    private JdbcTemplate jdb;
    private List<Order>comenzi;
    private  List<Order> comenzineprocesate;
    public OrderProcessing(JdbcTemplate jdb){
        this.jdb=jdb;
        comenzi=new ArrayList<>();
        comenzineprocesate=new ArrayList<>();

    }

    public void comenzileprocesate(){ //updateaza stocul la produse si proceseaza order din baza de date si vede daca poate fi efectuat sau nu
        OrderDao x=new OrderDao();
        DB_Product x2=new DB_Product();
        x.setJdbcTemplate(jdb);
        x2.setJdbcTemplate(jdb);
        List<Order> ord=x.allorders();
        List<Product> prd=x2.allProducts();
        boolean gasit=false;
        for(Order i:ord){
            gasit=false;
            for(Product j:prd){
                if(i.getProdus().equals(j.getNume()) && Integer.valueOf(i.getCantitate())<= Integer.valueOf(j.getCantitate())){

                    int cant=Integer.valueOf(j.getCantitate())-Integer.valueOf(i.getCantitate());
                    Product p=new Product(j.getNume(),""+cant,j.getPret());
                    x2.updateprod(p);
                    comenzi.add(i);
                    gasit=true;
                }

            }
            if(gasit==false)
                comenzineprocesate.add(i);
        }
    }

    public JdbcTemplate getJdb() {
        return jdb;
    }

    public void setJdb(JdbcTemplate jdb) {
        this.jdb = jdb;
    }

    public List<Order> getComenzi() {
        return comenzi;
    }

    public void setComenzi(List<Order> comenzi) {
        this.comenzi = comenzi;
    }

    public List<Order> getComenzineprocesate() {
        return comenzineprocesate;
    }

    public void setComenzineprocesate(List<Order> comenzineprocesate) {
        this.comenzineprocesate = comenzineprocesate;
    }
}
