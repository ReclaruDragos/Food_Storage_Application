package BusinesModel;

import Dao.DB_Product;
import Dao.DB_user;
import Dao.OrderDao;
import Model.Order;
import PresentationLawyer.InputUser;
import PresentationLawyer.OutputUser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class Main {
    public static void main(String []args) throws Exception {
        InputUser x=new InputUser();

        x.setFisier(args[0]);
        x.reader();
        String w=x.getT();




            ApplicationContext ctx=new ClassPathXmlApplicationContext("beans.xml");//luam jdbc din beans
        JdbcTemplate jdbcTemplate=(JdbcTemplate)ctx.getBean("jdbcTemplate");
//fac drop la tabelele din baza de date
            OrderDao dao=new OrderDao();
            DB_Product dbs=new DB_Product();
            DB_user u=new DB_user();
            dao.setJdbcTemplate(jdbcTemplate);
            dbs.setJdbcTemplate(jdbcTemplate);
            u.setJdbcTemplate(jdbcTemplate);
            dao.deleteAll();
            u.deleteAll();
            dbs.deleteAll();
        Parser pp=new Parser(w,jdbcTemplate);

       OrderProcessing ordPro=new OrderProcessing(jdbcTemplate);
        ordPro.comenzileprocesate();

        OutputUser outp=new OutputUser(jdbcTemplate);
        outp.writeBill(ordPro.getComenzi());
        outp.wirteRestul(ordPro.getComenzineprocesate());

        OrderUpdate up=new OrderUpdate(jdbcTemplate,ordPro.getComenzi());
        up.updateOrder();

    }
}
