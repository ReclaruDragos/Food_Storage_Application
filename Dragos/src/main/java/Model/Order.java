package Model;

public class Order {
    private String cumparator;
    private String produs;
    private String cantitate;

    public Order(){}

    public Order(String cumparator, String produs, String cantitate) {
        this.cumparator = cumparator;
        this.produs = produs;
        this.cantitate = cantitate;
    }

    public String getCumparator() {
        return cumparator;
    }

    public void setCumparator(String cumparator) {
        this.cumparator = cumparator;
    }

    public String getProdus() {
        return produs;
    }

    public void setProdus(String produs) {
        this.produs = produs;
    }

    public String getCantitate() {
        return cantitate;
    }

    public void setCantitate(String cantitate) {
        this.cantitate = cantitate;
    }
}
