package Model;

public class Product {
   private String nume;
   private String cantitate;
   private String pret;

public Product(){}

    public Product(String nume, String cantitate, String pret) {
        this.nume = nume;
        this.cantitate = cantitate;
        this.pret = pret;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getCantitate() {
        return cantitate;
    }

    public void setCantitate(String cantitate) {
        this.cantitate = cantitate;
    }

    public String getPret() {
        return pret;
    }

    public void setPret(String pret) {
        this.pret = pret;
    }
}
