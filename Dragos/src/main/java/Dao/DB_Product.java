package Dao;

import Model.Product;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

public class DB_Product {
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int saveProduct(Product e){

        String query="insert into produs values('"+e.getNume()+"','"+e.getCantitate()+"','"+e.getPret()+"');";


        return jdbcTemplate.update(query);
    }

    public int deleteProduct(Product e){
        String query="delete from produs where Nume='"+e.getNume()+"' ";
        return jdbcTemplate.update(query);
    }



    public int updateprod(Product p){


        int cantitate=0;
        for(Product i:allProducts()){
            if(i.getNume().equals(p.getNume())){

                deleteProduct(i);
                saveProduct(p);
            }
        }

        return 1;

    }

public int getPret(String numeProdus){
  for(Product i:allProducts())
      if(i.getNume().equals(numeProdus))
          return Integer.valueOf(i.getPret());

      return 0;
}
    public List<Product> allProducts() { //returneaza toate produsele
        String sql="Select * FROM produs";
        List<Product> p = jdbcTemplate.query(
                sql,
                new BeanPropertyRowMapper(Product.class));
        return p;
    }

    public int deleteAll(){
        String query="delete from produs";
        return jdbcTemplate.update(query);
    }



}
