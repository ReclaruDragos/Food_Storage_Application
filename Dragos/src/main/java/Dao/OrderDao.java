package Dao;

import Model.Order;
import Model.Product;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class OrderDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void saveOrder(Order e){


                    String query="insert into orders values('"+e.getCumparator()+"','"+e.getProdus()+"','"+e.getCantitate()+"');";
                    jdbcTemplate.update(query);
                }

    public List<Order> allorders() {
        String sql="Select * FROM orders";
        List<Order> p = jdbcTemplate.query(
                sql,
                new BeanPropertyRowMapper(Order.class));
        return p;
    }

    public int deleteAll(){
        String query="delete from orders";
        return jdbcTemplate.update(query);
    }





}
