package PresentationLawyer;

import Dao.DB_Product;
import Dao.DB_user;
import Dao.OrderDao;
import Model.Order;
import Model.Product;
import Model.User;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.jdbc.core.JdbcTemplate;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

public class OutputUser {
private  JdbcTemplate jdb;
private static int nrclient=0;
    private static int nro=0;
    private static int nrp=0;

    public OutputUser(JdbcTemplate jdb){
        this.jdb=jdb;
        nrclient++;
        nro++;
        nrp++;
    }

    public void writeBill(List<Order> l){
        DB_Product pd=new DB_Product();
        pd.setJdbcTemplate(jdb);
        String ss="nimic";
       String bill="bill";
   int k=1;

        try
        {
            for(Order i:l){

                Document document = new Document();
                String file=bill+k+".pdf";
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();
            int prettotal=Integer.valueOf(i.getCantitate())*pd.getPret(i.getProdus());
             ss=i.getCumparator()+" "+i.getProdus()+"X"+i.getCantitate()+" TOTAL:"+prettotal;
            document.add(new Paragraph(ss));


            document.close();
            k++;
            }

        } catch (DocumentException e)
        {
            e.printStackTrace();
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

    }

    public void wirteRestul(List<Order> l){



String ss="";
        try
        {


                Document document = new Document();

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("comenziNealocate.pdf"));
                document.open();
            for(Order i:l) {

                ss = i.getCumparator() + " " + i.getProdus() + " X " + i.getCantitate();
                document.add(new Paragraph(ss));
            }

                document.close();



        } catch (DocumentException e)
        {
            e.printStackTrace();
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }


    }

    public void reportClient(){

        DB_user uu=new DB_user();
        uu.setJdbcTemplate(jdb);
        Document document = new Document();
        try
        { String file="clientraport"+nrclient+".pdf";
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();

            PdfPTable table = new PdfPTable(2); // 3 columns.
            table.setWidthPercentage(100); //Width 100%
            table.setSpacingBefore(10f); //Space before table
            table.setSpacingAfter(10f); //Space after table




                for(User i:uu.allUsers()){
            PdfPCell cell1 = new PdfPCell(new Paragraph(i.getNume()));


            PdfPCell cell2 = new PdfPCell(new Paragraph(i.getAdresa()));




            table.addCell(cell1);
            table.addCell(cell2);
            }

            document.add(table);

            document.close();
            writer.close();
            nrclient++;


        } catch (Exception e)
        {
            e.printStackTrace();
        }



    }



    public void reportOrder(){

        OrderDao uu=new OrderDao();
        uu.setJdbcTemplate(jdb);
        Document document = new Document();
        try
        { String file="orderraport"+nro+".pdf";
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();

            PdfPTable table = new PdfPTable(3); // 3 columns.
            table.setWidthPercentage(100); //Width 100%
            table.setSpacingBefore(10f); //Space before table
            table.setSpacingAfter(10f); //Space after table




            for(Order i:uu.allorders()){
                PdfPCell cell1 = new PdfPCell(new Paragraph(i.getCumparator()));


                PdfPCell cell2 = new PdfPCell(new Paragraph(i.getProdus()));

                PdfPCell cell3 = new PdfPCell(new Paragraph(i.getCantitate()));




                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
            }

            document.add(table);

            document.close();
            writer.close();
            nro++;


        } catch (Exception e)
        {
            e.printStackTrace();
        }



    }

    public void reportProduct(){

        DB_Product uu=new DB_Product();
        uu.setJdbcTemplate(jdb);
        Document document = new Document();
        try
        { String file="productraport"+nrp+".pdf";
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();

            PdfPTable table = new PdfPTable(3); // 3 columns.
            table.setWidthPercentage(100); //Width 100%
            table.setSpacingBefore(10f); //Space before table
            table.setSpacingAfter(10f); //Space after table




            for(Product i:uu.allProducts()){
                PdfPCell cell1 = new PdfPCell(new Paragraph(i.getNume()));


                PdfPCell cell2 = new PdfPCell(new Paragraph(i.getCantitate()));
                PdfPCell cell3 = new PdfPCell(new Paragraph(i.getPret()));




                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
            }

            document.add(table);

            document.close();
            writer.close();
            nrp++;


        } catch (Exception e)
        {
            e.printStackTrace();
        }



    }

}
